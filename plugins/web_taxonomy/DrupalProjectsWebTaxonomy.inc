<?php

class DrupalProjectsWebTaxonomy extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   */
  public function autocomplete($string = '') {
    $matches = array();
    $web_tids = array();

    $url = "http://api.scraperwiki.com/api/1.0/datastore/sqlite";
    $params = array(
      'format' => 'jsondict',
      'name' => 'drupaldocweb',
      'query' => "select * from `swdata` where project_title LIKE '%%$string%%'",
    );

    return $this->fetchTermInfo($url, $params);
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   */
  public function fetchTerm($term) {
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    if (!empty($web_tid)) {
      $url = "http://api.scraperwiki.com/api/1.0/datastore/sqlite";
      $params = array(
        'format' => 'jsondict',
        'name' => 'drupaldocweb',
        'query' => "select * from `swdata` where uri = '$web_tid'",
      );
    }
    else {
      // @todo If there is no Web Tid, check by name.
    }
    $term_info = $this->fetchTermInfo($url, $params);
    return array_shift($term_info);
  }

  protected function fetchTermInfo($url, $params) {
    $term_info = array();

    $http = http_client();
    $data = $http->get($url, $params);

    //$data = temp_data();
    $projects = json_decode($data);
    foreach ($projects as $project) {
      $term_info[$project->title] = array(
        'name' => $project->title,
        'web_tid' => $project->uri,
        'parent' => $project->parent,
      );
    }

    return $term_info;
  }
}

/**
 * Function for testing when there is no Internet connection. 
 */
function temp_data() {
  return '[
    {
        "title": "apachesolr_attachments",
        "uri": "http://git.drupal.org/project/apachesolr_attachments.git"
    },
    {
        "title": "apachesolr_autocomplete",
        "uri": "http://git.drupal.org/project/apachesolr_autocomplete.git"
    },
    {
        "title": "apachesolr_multilingual",
        "uri": "http://git.drupal.org/project/apachesolr_multilingual.git"
    },
    {
        "title": "apachesolr_og",
        "uri": "http://git.drupal.org/project/apachesolr_og.git"
    },
    {
        "title": "apachesolr_page",
        "uri": "http://git.drupal.org/project/apachesolr_page.git"
    },
    {
        "title": "apachesolr",
        "uri": "http://git.drupal.org/project/apachesolr.git"
    },
    {
        "title": "apachesolr_stats",
        "uri": "http://git.drupal.org/project/apachesolr_stats.git"
    },
    {
        "title": "apachesolr_views",
        "uri": "http://git.drupal.org/project/apachesolr_views.git"
    }
]';
}